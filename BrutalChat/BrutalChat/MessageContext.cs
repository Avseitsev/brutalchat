﻿using Microsoft.EntityFrameworkCore;


namespace BrutalChat
{
    public class MessageContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UsersConnectcs> Connects { get; set; }

        public MessageContext(DbContextOptions<MessageContext> options)
            : base(options)
        {
        }
    }
}
