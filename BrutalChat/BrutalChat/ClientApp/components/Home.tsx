﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { HubConnection } from '@aspnet/signalr-client';
import axios from 'axios';
import { Tabs, Tab } from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';
import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';
import RaisedButton from 'material-ui/RaisedButton';
import ActionAndroid from 'material-ui/svg-icons/action/android';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import { GridList, GridTile } from 'material-ui/GridList';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import AutoComplete from 'material-ui/AutoComplete';

interface HomeState {
    statusConnect: string;
    name: string | null;
    loadStatus: boolean;
    dataSource: Array<any>;
    messages: Array<IMessage>;
    messagesStory: Array<IMessage>;
}

interface IMessage {
    time: string|null;
    name: string | null;
    message: string;
    type: number;//0 - normal 1 - in 2 - out 3 - only message
}

class Message extends React.Component<{ message: IMessage }, {}>{

    render() {
        if (this.props.message.type == 3) {
            return <li>{this.props.message.message}</li>
        }
        if (this.props.message.type == 2) {
            return <li style={{ color: "red" }}><strong>{this.props.message.time}  U->{this.props.message.name}</strong>: {this.props.message.message}</li>
        }
        if (this.props.message.type == 1) {
            return <li style={{ color: "green" }}><strong>{this.props.message.time}  {this.props.message.name}->U</strong>: {this.props.message.message}</li>
        }
        if (this.props.message.type == 0) {
            return <li><strong>{this.props.message.time}->{this.props.message.name}</strong>:{this.props.message.message}</li>
        }
        else {
            return <li>error</li>
        }

    }
}


let connection = new HubConnection('/message');

export class Home extends React.Component<RouteComponentProps<{}>, HomeState> {
    constructor() {
        super();
        this.state = { statusConnect: "false", name: null, loadStatus: false, dataSource: [], messages: [], messagesStory: []  };
    }


    componentDidMount() {

        var z = this;
        axios.get("api/Account" + "/log?firstName=" + localStorage.getItem('name') + "&password=" + localStorage.getItem('pass'))
            .then(function (response) {
                if (response.data == true) {
                    z.setState({ name: localStorage.getItem('name') });
                    z.connect();
                }
                z.setState({ loadStatus: true });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillUnmount() {
        connection.send('newUser', "user out -> " + this.state.name);
    }


    connect() {

        connection.on('send2', (message, name, time) => {
            var type = 1;
            this.setState({ messages: this.state.messages.concat({ time, name, message, type }) });

        });

        connection.on('send', (message, name, time) => {
            var type = 0;
            this.setState({ messages: this.state.messages.concat({ time, name, message,type }) });
        });

        connection.on('newUser', (message) => {
            var type = 3;
            this.setState({ messages: this.state.messages.concat({ time:null, name:null, message, type}) });
        });

        connection.start()
            .then(() => {
                console.log('MessageHub Connected');
                connection.send('saveId', this.state.name);
                this.setState({ statusConnect: "true" });
            });

        connection.onclose(e => {
            connection.send('newUser', "user out -> " + this.state.name);
        })

        axios.get("api/Message" + "/getStory")
            .then(response => {
                const mappedData: IMessage[] = response.data.map(apiMessage => {
                    return { ...apiMessage, type: 0 };
                });

                this.setState({ messagesStory: mappedData });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    send() {
        if ((document.getElementById("toName") as HTMLInputElement).value == "") {
            connection.send('send', (document.getElementById("message") as HTMLInputElement).value, this.state.name, new Date().toLocaleTimeString());
        } else {
            connection.send('send2', (document.getElementById("message") as HTMLInputElement).value, (document.getElementById("toName") as HTMLInputElement).value, this.state.name, new Date().toLocaleTimeString());
            var type = 2;
            this.setState({ messages: this.state.messages.concat({ time: new Date().toLocaleTimeString(), name: (document.getElementById("toName") as HTMLInputElement).value, message: (document.getElementById("message") as HTMLInputElement).value, type }) });
        }
    }

    sendName() {
        var z = this;
        axios.get("api/Account" + "/log?firstName=" + (document.getElementById("name") as HTMLInputElement).value + "&password=" + (document.getElementById("password") as HTMLInputElement).value)
            .then(function (response) {
                if (response.data == true) {
                    localStorage.setItem('name', (document.getElementById("name") as HTMLInputElement).value);
                    localStorage.setItem('pass', (document.getElementById("password") as HTMLInputElement).value);
                    z.setState({ name: (document.getElementById("name") as HTMLInputElement).value });
                    z.connect();
                    connection.send('saveId', z.state.name);
                    connection.send('newUser', "a new user joined the chat -> " + z.state.name);
                }
                else {
                    alert("wrong login or password");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    registration() {
        var z = this;
        axios.get("api/Account" + "/reg?firstName=" + (document.getElementById("namereg") as HTMLInputElement).value + "&password=" + (document.getElementById("passwordreg") as HTMLInputElement).value + "&password2=" + (document.getElementById("reppasswordreg") as HTMLInputElement).value)
            .then(function (response) {
                if (response.data == true) {
                    alert("OK! U CAN LOG IN NOW");
                }
                else {
                    alert(response.data);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    handleUpdateInput = (value) => {
        var z = this;
        axios.get("api/Account" + "/top?name=" + (document.getElementById("toName") as HTMLInputElement).value)
            .then(function (response) {
                z.setState({ dataSource: response.data });
            })
            .catch(function (error) {
                console.log(error);
            });
    };


    out() {
        connection.send('newUser', "user out -> " + this.state.name);
        localStorage.removeItem('name');
        localStorage.removeItem('pass');
        this.setState({ name: null });
    }


    public render() {
        return <div>

            {/*--кусок контента--*/}
            {this.state.name != null &&
                <div>
                    <AppBar title="MENU" iconElementRight={<FlatButton label="OUT" onClick={this.out.bind(this)} />} />
                    <div className="container">

                        {this.state.statusConnect == "false" &&
                            <div>
                                <input type="text" id="message" />
                                <input type="button" id="sendmessage" value="Send" disabled style={{ background: 'red' }} />
                            </div>
                        }
                        {this.state.statusConnect == "true" &&
                            <div>
                                <input type="text" id="message" />
                                <input type="button" id="sendmessage" value="Send" style={{ background: 'green' }} onClick={this.send.bind(this)} />
                                <label>To Name</label>
                                <AutoComplete id="toName" hintText="Type anything" dataSource={this.state.dataSource} onUpdateInput={this.handleUpdateInput} />
                            </div>
                        }

                        <ul id="story" style={{ height: "300", overflow: "auto" }}> {this.state.messagesStory.map((value) => <Message message={value} />)}</ul>
                        <ul id="discussion" style={{ height: "400", overflow: "auto" }}> {this.state.messages.map((value) => <Message message={value} />)} </ul>                     
                    </div>
                </div>
            }


            {/*--кусок для окна реги/входу--*/}
            {this.state.loadStatus == false &&
                <div>
                    <RefreshIndicator size={40} left={10} top={0} status="loading" />
                </div>}

            {this.state.loadStatus == true &&
                <div>
                    {this.state.name == null &&
                        <Tabs>

                            <Tab icon={<MapsPersonPin />} label="LOGG IN" >
                                <div className="container">
                                    <h1>ENTER NAME</h1>
                                    <input type="text" id="name" />
                                    <h1>PASSWORD</h1>
                                    <input id="password" type="Password" />
                                    <br />
                                    <br />
                                    <RaisedButton label="LOGG IN" primary={true} onClick={this.sendName.bind(this)} />
                                </div>
                            </Tab>

                            <Tab icon={<MapsPersonPin />} label="REGISTRATION" >
                                <div className="container">
                                    <h1>ENTER NAME</h1>
                                    <input type="text" id="namereg" />
                                    <h1>PASSWORD</h1>
                                    <input id="passwordreg" type="Password" />
                                    <h1>REPEAT PASSWORD</h1>
                                    <input id="reppasswordreg" type="Password" />
                                    <br />
                                    <br />
                                    <RaisedButton label="REGISTRATION" primary={true} onClick={this.registration.bind(this)} />
                                </div>
                            </Tab>

                        </Tabs>}
                </div>}


        </div>;
    }
}
