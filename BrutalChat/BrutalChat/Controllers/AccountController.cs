﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BrutalChat.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private MessageContext db;
        public AccountController(MessageContext context)
        {
            db = context;
        }



        [HttpGet("reg")]
        public string Get(string firstName, string password, string password2)
        {
            try
            {
                if (firstName != null && password != null && password2 != null && password == password2)
                {
                    User user = db.Users.FirstOrDefault(u => u.Email == firstName);
                    if (user == null)
                    {
                        db.Users.Add(new User { Email = firstName, Password = password });
                        db.SaveChanges();

                        return "true";
                    }
                    else
                    {
                        return "LOGIN BUSY";
                    }
                }
                else
                {
                    return "NULL INPUT";
                }
            }
            catch
            {
                return "SORRY SERVER ERROR";
            }
        }

        [HttpGet("log")]
        public string Get(string firstName, string password)
        {
            if (firstName != null && password != null)
            {
                User user = db.Users.FirstOrDefault(u => u.Email == firstName && u.Password == password);
                if (user != null)
                {
                    Logout();
                    Authenticate(firstName);
                    return "true";
                }
                else
                {
                    return "false";
                }
            }
            else
            {
                return "false";
            }
        }



        [HttpGet("top")]
        public List<string> Top(string name)
        {
            try
            {               
                List<string> list = db.Connects.Where(e => e.Email.ToUpper().StartsWith(name.ToUpper())).Select(e=>e.Email).Take(5).ToList();
                return list;
            }
            catch
            {
                return null;
            }
        }


        private void Authenticate(string userName)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public void Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}
