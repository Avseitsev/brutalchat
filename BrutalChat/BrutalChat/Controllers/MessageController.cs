﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BrutalChat.Controllers
{
    [Route("api/[controller]")]
    public class MessageController : Controller
    {
        private IHubContext<MessageHub> _messageHubContext;
        private MessageContext db;

        public MessageController(IHubContext<MessageHub> messageHubContext, MessageContext context)
        {
            _messageHubContext = messageHubContext;
            db = context;
        }

        [HttpGet("getStory")]
        public List<Message> Get()
        {
            return db.Messages.ToList();
        }

        public IActionResult Post()
        {
            //Broadcast message to client  
            _messageHubContext.Clients.All.InvokeAsync("send", "Hello from the hub server at " +
            DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));

            return Ok();
        }
    }
}
