﻿using Microsoft.AspNetCore.SignalR;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrutalChat
{
    public class MessageHub : Hub
    {
        private MessageContext db;
        public MessageHub(MessageContext context)
        {
            db = context;
        }

        public Task Send2(string message, string name,string nameFrom,string time)
        {
            try
            {
                var id = db.Connects.FirstOrDefault(e => e.Email == name).Connect;
                return Clients.Client(id).InvokeAsync("Send2", message, nameFrom, time);
            }
            catch
            {
                var id = Context.ConnectionId;
                return Clients.Client(id).InvokeAsync("Send2", "NO USER");
            }
        }


        public void SaveId(string name)
        {

            if (db.Connects.FirstOrDefault(e=>e.Email==name)==null) {
                var id = Context.ConnectionId;
                db.Connects.Add(new UsersConnectcs { Email = name, Connect = id });
            }
            else
            {
                db.Connects.FirstOrDefault(e => e.Email == name).Connect = Context.ConnectionId;
            }
            db.SaveChanges();

        }


        public Task NewUser(string message)
        {
            return Clients.All.InvokeAsync("NewUser", message);
        }

        public Task Send(string message,string name, string time)
        {
            db.Messages.Add(new Message { MessageText= message, Name= name, Time= time });
            db.SaveChanges();
            return Clients.All.InvokeAsync("Send", message,name,time);
        }
    }
}
